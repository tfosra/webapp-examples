/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package com.tfosra.restau.middleware.rest;

import com.tfosra.restau.backend.srv.itf.IBaseSRV;
import org.json.JSONObject;

/**
 *
 * @author ShadowHacker
 */
public class BaseService {
    
    public JSONObject oJSONObject = new JSONObject();
    
    public String buildParams(String error, String message) {
        oJSONObject.put("err_code", error);
        oJSONObject.put("message", message);
        
        return oJSONObject.toString();
    }
    
    public String buildError(IBaseSRV srv, Throwable e) {
        JSONObject json = new JSONObject();
        json.put("err_code", srv.getError());
        json.put("success", false);
        json.put("message", e.getMessage());
        return json.toString();
    }
    
    public String buildParamsMessageJson(String error, JSONObject ooJSONObject) {
        
        oJSONObject.put("err_code", error);
        oJSONObject.put("message", ooJSONObject);
        
        return oJSONObject.toString();
    }
    
}
