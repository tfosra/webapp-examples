/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package com.tfosra.restau.middleware.rest;

import com.tfosra.restau.backend.srv.itf.IObjObjectSRV;
import javax.ws.rs.Consumes;
import javax.ws.rs.FormParam;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import org.apache.log4j.Logger;
import org.springframework.stereotype.Service;

/**
 *
 * @author ShadowHacker
 */
@Service("ObjectService")
@Path("/object")
public class ObjObjectService extends BaseService {
    
    public static Logger log = Logger.getLogger(ObjObjectService.class.getName());
    
    IObjObjectSRV objObjectSRV = null;

    public void setObjObjectSRV(IObjObjectSRV objObjectSRV) {
        this.objObjectSRV = objObjectSRV;
    }
    
    @POST
    @Path("/get")
    @Consumes({"application/x-www-form-urlencoded"})
    @Produces({"application/json"})
    public String get(@FormParam("callback") String jsonp) {
        try {
            objObjectSRV.get();
            String res = buildParams(objObjectSRV.getError(), objObjectSRV.getMessage());
            if (jsonp != null)
                return jsonp + "("+res+");";
            return res;
        } catch (Exception ex) {
            return buildError(objObjectSRV, ex);
        }
    }
    
}
