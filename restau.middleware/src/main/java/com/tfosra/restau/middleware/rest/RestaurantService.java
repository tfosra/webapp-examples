/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package com.tfosra.restau.middleware.rest;

import com.tfosra.restau.backend.srv.itf.IRestaurantSRV;
import javax.ws.rs.Consumes;
import javax.ws.rs.FormParam;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import org.apache.log4j.Logger;
import org.springframework.stereotype.Service;

/**
 *
 * @author ShadowHacker
 */
@Service("RestaurantService")
@Path("/restaurant")
public class RestaurantService extends BaseService {
    
    public static Logger log = Logger.getLogger(RestaurantService.class.getName());
    
    IRestaurantSRV restaurantSRV = null;

    public void setRestaurantSRV(IRestaurantSRV restaurantSRV) {
        this.restaurantSRV = restaurantSRV;
    }
    
    @POST
    @Path("/create")
    @Consumes({"application/x-www-form-urlencoded"})
    @Produces({"application/json"})
    public String create(@FormParam("jsonParam") String jsonParam, @FormParam("callback") String jsonp) {
        try {
            restaurantSRV.create(jsonParam);
            String res = buildParams(restaurantSRV.getError(), restaurantSRV.getMessage());
            if (jsonp != null)
                return jsonp + "("+res+");";
            return res;
        } catch (Exception ex) {
            return buildError(restaurantSRV, ex);
        }
    }
    
    @POST
    @Path("/update")
    @Consumes({"application/x-www-form-urlencoded"})
    @Produces({"application/json"})
    public String update(@FormParam("id") String intId, @FormParam("jsonParam") String jsonParam, @FormParam("callback") String jsonp) {
        try {
            restaurantSRV.update(intId, jsonParam);
            String res = buildParams(restaurantSRV.getError(), restaurantSRV.getMessage());
            if (jsonp != null)
                return jsonp + "("+res+");";
            return res;
        } catch (Exception ex) {
            return buildError(restaurantSRV, ex);
        }
    }
    
    @POST
    @Path("/delete")
    @Consumes({"application/x-www-form-urlencoded"})
    @Produces({"application/json"})
    public String delete(@FormParam("id") String intId, @FormParam("callback") String jsonp) {
        try {
            restaurantSRV.delete(intId);
            String res = buildParams(restaurantSRV.getError(), restaurantSRV.getMessage());
            if (jsonp != null)
                return jsonp + "("+res+");";
            return res;
        } catch (Exception ex) {
            return buildError(restaurantSRV, ex);
        }
    }
    
    @POST
    @Path("/getbyid")
    @Consumes({"application/x-www-form-urlencoded"})
    @Produces({"application/json"})
    public String getById(@FormParam("id") String intId, @FormParam("callback") String jsonp) {
        try {
            restaurantSRV.getById(intId);
            String res = buildParams(restaurantSRV.getError(), restaurantSRV.getMessage());
            if (jsonp != null)
                return jsonp + "("+res+");";
            return res;
        } catch (Exception ex) {
            return buildError(restaurantSRV, ex);
        }
    }
    
    @POST
    @Path("/get")
    @Consumes({"application/x-www-form-urlencoded"})
    @Produces({"application/json"})
    public String getAll(@FormParam("callback") String jsonp) {
        try {
            restaurantSRV.get();
            String res = buildParams(restaurantSRV.getError(), restaurantSRV.getMessage());
            if (jsonp != null)
                return jsonp + "("+res+");";
            return res;
        } catch (Exception ex) {
            return buildError(restaurantSRV, ex);
        }
    }
    
}
