/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.tfosra.restau.web.controller;

import com.tfosra.restau.web.bean.BaseEntityBean;
import com.tfosra.restau.web.bean.ObjObjectBean;
import com.tfosra.restau.web.bean.ObjectPropertyBean;
import com.tfosra.restau.web.bean.ObjectPropertyValueBean;
import com.tfosra.restau.web.bean.RestaurantBean;
import java.lang.reflect.Method;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.PropertyResourceBundle;
import javax.annotation.PostConstruct;
import javax.faces.context.FacesContext;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;
import org.json.JSONArray;
import org.json.JSONObject;

/**
 *
 * @author ShadowHacker
 */
public class BaseController {

    public final static PropertyResourceBundle settingBundle = (PropertyResourceBundle) PropertyResourceBundle.getBundle("RestoJSF");
    protected static String server = settingBundle.getString("rest.resto.middleware.server");
    protected static String address = settingBundle.getString("rest.resto.middleware.server.address");

    HttpSession session;

    @PostConstruct
    private void init() {
        session = (HttpSession) FacesContext.getCurrentInstance()
                .getExternalContext().getSession(false);
    }

    protected final HttpServletRequest getRequest() {
        return (HttpServletRequest) FacesContext.getCurrentInstance()
                .getExternalContext().getRequest();
    }


    protected final <T extends BaseEntityBean> List<T> parseEntityListFromJson(String strJson, Class<T> type) throws Exception {
        List<T> lst = new ArrayList<>();
        JSONArray jsonArray = new JSONArray(strJson);
        for (int i = 0; i < jsonArray.length(); i++) {
            lst.add(parseEntityFromJson(jsonArray.getString(i), type));
        }
        return lst;
    }

    protected final <T extends BaseEntityBean> T parseEntityFromJson(String strJson, Class<T> type) throws Exception {
        T oEntityBean = null;

        String objectName = type.getSimpleName();
        String methodName = "parse" + objectName + "FromJson";
        JSONObject json = new JSONObject(strJson);

        try {
            Method buildMethod = BaseController.class.getDeclaredMethod(methodName, JSONObject.class);
            oEntityBean = (T) buildMethod.invoke(this, json);
        } catch (Exception e) {
            e.printStackTrace();
        }
        // Cette instruction doit être la dernière de la méthode
        if (oEntityBean != null) {
            parseBaseEntityFromJson(json, oEntityBean);
        }
        return oEntityBean;
    }

    private <T extends BaseEntityBean> void parseBaseEntityFromJson(JSONObject jsonObject, T oEntityBean) throws Exception {
        oEntityBean.setId(jsonObject.getInt("id"));

        List<ObjectPropertyValueBean> lstOfValues = parseEntityListFromJson(jsonObject.optString("lstOfObjectPropertyValue", "[]"), ObjectPropertyValueBean.class);
        if (lstOfValues != null) {
            Map<String, ObjectPropertyValueBean> map = oEntityBean.getMapOfPropertyValue();
            map.clear();
            for (ObjectPropertyValueBean val : lstOfValues) {
                map.put(val.getObjectPropertyName(), val);
            }
        }
    }

    private RestaurantBean parseRestaurantBeanFromJson(JSONObject json) throws Exception {
        RestaurantBean r = new RestaurantBean();
        r.setStrName(json.getString("strName"));
        r.setStrDescription(json.getString("strDescription"));
        return r;
    }
    
    private ObjObjectBean parseObjObjectBeanFromJson(JSONObject json) throws Exception {
        ObjObjectBean o = new ObjObjectBean();
        o.setStrName(json.getString("strName"));
        
        List<ObjectPropertyBean> lstOfProperties = parseEntityListFromJson(json.optString("lstOfObjectProperty", "[]"), ObjectPropertyBean.class);
        o.setLstOfObjectProperty(lstOfProperties);
        return o;
    }
    
    private ObjectPropertyBean parseObjectPropertyBeanFromJson(JSONObject json) throws Exception {
        ObjectPropertyBean o = new ObjectPropertyBean();
        o.setObjObjectId(json.getInt("objObjectId"));
        o.setStrProperty(json.getString("strProperty"));
        return o;
    }

    private ObjectPropertyValueBean parseObjectPropertyValueBeanFromJson(JSONObject json) throws Exception {
        ObjectPropertyValueBean o = new ObjectPropertyValueBean();
        o.setIntItem(json.getInt("intItem"));
        o.setObjectPropertyId(json.getInt("objectPropertyId"));
        o.setStrValue(json.optString("strValue", null));
        return o;
    }

    protected Object fromViewScope(String key) {
        return FacesContext.getCurrentInstance().getViewRoot().getViewMap().get(key);
    }

    protected Object fromRequestScope(String key) {
        return getRequest().getAttribute(key);
    }

    protected final boolean emptyIdentifier(String intEntityId) {
        try {
            int val = Integer.valueOf(intEntityId);
            return val <= 0;
        } catch (Exception e) {
            return true;
        }
    }

}
