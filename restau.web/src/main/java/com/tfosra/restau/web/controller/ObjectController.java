/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.tfosra.restau.web.controller;

import com.tfosra.restau.web.bean.ObjObjectBean;
import java.util.ArrayList;
import java.util.List;
import javax.annotation.PostConstruct;
import org.apache.http.client.fluent.Form;
import org.apache.http.client.fluent.Request;
import org.json.JSONObject;

/**
 *
 * @author ShadowHacker
 */
public class ObjectController extends BaseController {
    
    private ObjObjectBean objectBean;
    private List<ObjObjectBean> lstOfObjObjectBean;
    
    @PostConstruct
    private void init() {
        objectBean = new ObjObjectBean();
    }

    public ObjObjectBean getObjectBean() {
        return objectBean;
    }

    public void setObjectBean(ObjObjectBean objectBean) {
        this.objectBean = objectBean;
    }

    public List<ObjObjectBean> getLstOfObjObjectBean() {
        if (lstOfObjObjectBean == null) {
            lstOfObjObjectBean = new ArrayList<>();
            try {
                String url = server + address + "object/get";
                String str_Response = Request.Post(url)
                        .bodyForm(Form.form()
                                .build())
                        .execute().returnContent().asString();
                JSONObject oJSONObject = new JSONObject(str_Response);
                if (oJSONObject.getString("err_code").equals("1")) {
                    lstOfObjObjectBean.addAll(parseEntityListFromJson(oJSONObject.getString("message"), ObjObjectBean.class));
                }
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
        return lstOfObjObjectBean;
    }

    public void setLstOfObjObjectBean(List<ObjObjectBean> lstOfObjObjectBean) {
        this.lstOfObjObjectBean = lstOfObjObjectBean;
    }
    
    public ObjObjectBean getByName(String objectName) {
        ObjObjectBean o = new ObjObjectBean();
        o.setStrName(objectName);
        List<ObjObjectBean> lstOfObj = getLstOfObjObjectBean();
        int index = lstOfObj.indexOf(o);
        if (index != -1) {
            return lstOfObj.get(index);
        }
        return null;
    }
    
}
