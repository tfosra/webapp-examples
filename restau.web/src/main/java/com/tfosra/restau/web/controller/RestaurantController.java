/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package com.tfosra.restau.web.controller;

import com.tfosra.restau.web.bean.ObjObjectBean;
import com.tfosra.restau.web.bean.ObjectPropertyValueBean;
import com.tfosra.restau.web.bean.RestaurantBean;
import static com.tfosra.restau.web.controller.BaseController.server;
import java.util.ArrayList;
import java.util.List;
import javax.annotation.PostConstruct;
import org.apache.http.client.fluent.Form;
import org.apache.http.client.fluent.Request;
import org.json.JSONObject;

/**
 *
 * @author ShadowHacker
 */
public class RestaurantController extends BaseController {
    
    private RestaurantBean restaurantBean;
    private List<RestaurantBean> lstOfRestaurantBean;

    public RestaurantBean getRestaurantBean() {
        return restaurantBean;
    }

    public void setRestaurantBean(RestaurantBean restaurantBean) {
        this.restaurantBean = restaurantBean;
    }
    
    @PostConstruct
    private void init() {
        restaurantBean = new RestaurantBean();
    }

    public List<RestaurantBean> getLstOfRestaurantBean() {
        if (lstOfRestaurantBean == null) {
            lstOfRestaurantBean = new ArrayList<>();
            try {
                String url = server + address + "restaurant/get";
                String str_Response = Request.Post(url)
                        .bodyForm(Form.form()
                                .build())
                        .execute().returnContent().asString();
                JSONObject oJSONObject = new JSONObject(str_Response);
                if (oJSONObject.getString("err_code").equals("1")) {
                    lstOfRestaurantBean.addAll(parseEntityListFromJson(oJSONObject.getString("message"), RestaurantBean.class));
                }
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
        return lstOfRestaurantBean;
    }
    
    public void reloadList() {
        setLstOfRestaurantBean(null);
        getLstOfRestaurantBean();
    }

    public void setLstOfRestaurantBean(List<RestaurantBean> lstOfRestaurantBean) {
        this.lstOfRestaurantBean = lstOfRestaurantBean;
    }
    
    public String startRestaurantUpdate(RestaurantBean restaurant) {
        setRestaurantBean(restaurant);
        return "/restaurant/update.xhtml";
    }
    
    public String startRestaurantView(RestaurantBean restaurant) {
        setRestaurantBean(restaurant);
        return "/restaurant/view.xhtml";
    }
    
    public String startRestaurantCreate() {
        setRestaurantBean(new RestaurantBean());
        return "/restaurant/create.xhtml";
    }
    
    public String create() {
        try {
            String url = server + address + "restaurant/create";
            
            JSONObject jsonParam = new JSONObject();
            jsonParam.put("strName", restaurantBean.getStrName());
            jsonParam.put("strDescription", restaurantBean.getStrDescription());
            
            ObjectPropertyValueBean propValue;
            for (String strProperty : restaurantBean.getMapOfPropertyValue().keySet()) {
                propValue = restaurantBean.getMapOfPropertyValue().get(strProperty);
                jsonParam.put(strProperty, propValue.getStrValue());
            } 
            
            String str_Response = Request.Post(url)
                    .bodyForm(Form.form()
                            .add("jsonParam", jsonParam.toString())
                            .build())
                    .execute().returnContent().asString();
            JSONObject oJSONObject = new JSONObject(str_Response);
            if (oJSONObject.getString("err_code").equals("1")) {
                lstOfRestaurantBean.add(parseEntityFromJson(oJSONObject.getString("message"), RestaurantBean.class));
                return "/restaurant/list.xhtml?faces-redirect=true";
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return null;
    }
    
    public void update() {
        try {
            String url = server + address + "restaurant/create";
            
            JSONObject jsonParam = new JSONObject();
            jsonParam.put("strName", restaurantBean.getStrName());
            jsonParam.put("strDescription", restaurantBean.getStrDescription());
            
            ObjectPropertyValueBean propValue;
            for (String strProperty : restaurantBean.getMapOfPropertyValue().keySet()) {
                propValue = restaurantBean.getMapOfPropertyValue().get(strProperty);
                jsonParam.put(strProperty, propValue.getStrValue());
            } 
            
            String str_Response = Request.Post(url)
                    .bodyForm(Form.form()
                            .add("restaurantId", JSONObject.numberToString(restaurantBean.getId()))
                            .add("jsonParam", jsonParam.toString())
                            .build())
                    .execute().returnContent().asString();
            JSONObject oJSONObject = new JSONObject(str_Response);
            if (oJSONObject.getString("err_code").equals("1")) {
                lstOfRestaurantBean.addAll(parseEntityListFromJson(oJSONObject.getString("message"), RestaurantBean.class));
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
    
}
