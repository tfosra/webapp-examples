/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.tfosra.restau.web.bean;

/**
 *
 * @author ShadowHacker
 */
public class ObjectPropertyValueBean extends BaseEntityBean {
    
    private String strValue;
    private int intItem;
    private int objectPropertyId;
    private String objectPropertyName;

    public String getObjectPropertyName() {
        return objectPropertyName;
    }

    public void setObjectPropertyName(String objectPropertyName) {
        this.objectPropertyName = objectPropertyName;
    }

    public int getObjectPropertyId() {
        return objectPropertyId;
    }

    public void setObjectPropertyId(int objectPropertyId) {
        this.objectPropertyId = objectPropertyId;
    }

    public String getStrValue() {
        return strValue;
    }

    public void setStrValue(String strValue) {
        this.strValue = strValue;
    }

    public int getIntItem() {
        return intItem;
    }

    public void setIntItem(int intItem) {
        this.intItem = intItem;
    }
    
}
