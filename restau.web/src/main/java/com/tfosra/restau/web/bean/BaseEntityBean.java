/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.tfosra.restau.web.bean;

import java.util.HashMap;
import java.util.Map;

/**
 *
 * @author ShadowHacker
 */
public class BaseEntityBean {

    private int id;
    protected String objectName;
    private Map<String, ObjectPropertyValueBean> mapOfPropertyValue;

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getObjectName() {
        return objectName;
    }

    public void setObjectName(String objectName) {
        this.objectName = objectName;
    }

    public Map<String, ObjectPropertyValueBean> getMapOfPropertyValue() {
        if (mapOfPropertyValue == null) {
            mapOfPropertyValue = new HashMap<>();
        }
        return mapOfPropertyValue;
    }

    @Override
    public boolean equals(Object obj) {
        if (obj == null || !getClass().isAssignableFrom(obj.getClass())) {
            return false;
        }
        BaseEntityBean b = (BaseEntityBean)obj;
        return ((id > 0 || b.id > 0) && id == b.id) || this == b;
    }

}
