/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.tfosra.restau.web.bean;

import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author ShadowHacker
 */
public class ObjObjectBean extends BaseEntityBean {
    
    private String strName;
    private List<ObjectPropertyBean> lstOfObjectProperty;

    public String getStrName() {
        return strName;
    }

    public void setStrName(String strName) {
        this.strName = strName;
    }

    public List<ObjectPropertyBean> getLstOfObjectProperty() {
        if (lstOfObjectProperty == null) {
            lstOfObjectProperty = new ArrayList<>();
        }
        return lstOfObjectProperty;
    }

    public void setLstOfObjectProperty(List<ObjectPropertyBean> lstOfObjectProperty) {
        this.lstOfObjectProperty = lstOfObjectProperty;
    }

    @Override
    public boolean equals(Object obj) {
        boolean res = super.equals(obj);
        if (obj != null && obj instanceof ObjObjectBean && strName != null) {
            ObjObjectBean o = (ObjObjectBean) obj;
            res |= strName.equals(o.strName);
        }
        return res;
    }
    
}
