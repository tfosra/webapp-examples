/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.tfosra.restau.web.bean;

/**
 *
 * @author ShadowHacker
 */
public class ObjectPropertyBean extends BaseEntityBean {
    
    private String strProperty;
    private int objObjectId;

    public String getStrProperty() {
        return strProperty;
    }

    public void setStrProperty(String strProperty) {
        this.strProperty = strProperty;
    }

    public int getObjObjectId() {
        return objObjectId;
    }

    public void setObjObjectId(int objObjectId) {
        this.objObjectId = objObjectId;
    }
    
}
