/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.tfosra.restau.web.converter;

import com.tfosra.restau.web.bean.BaseEntityBean;
import com.tfosra.restau.web.bean.ObjectPropertyValueBean;
import javax.faces.component.UIComponent;
import javax.faces.context.FacesContext;
import javax.faces.convert.Converter;
import javax.faces.convert.FacesConverter;

/**
 *
 * @author ShadowHacker
 */
@FacesConverter("propertyValueConverter")
public class PropertyValueConverter implements Converter{

    @Override
    public Object getAsObject(FacesContext context, UIComponent component, String value) {
        BaseEntityBean oEntity = (BaseEntityBean) component.getAttributes().get("entityBean");
        String strProperty = (String) component.getAttributes().get("strProperty");
        
        ObjectPropertyValueBean propertyValue = oEntity.getMapOfPropertyValue().get(strProperty);
        
        if (propertyValue == null) {
            propertyValue = new ObjectPropertyValueBean();
            propertyValue.setIntItem(oEntity.getId());
            propertyValue.setObjectPropertyName(strProperty);
        }
        propertyValue.setStrValue(value);
        
        return propertyValue;
    }

    @Override
    public String getAsString(FacesContext context, UIComponent component, Object value) {
        ObjectPropertyValueBean propertyValue = (ObjectPropertyValueBean) value;
        
        if (propertyValue == null) {
            propertyValue = new ObjectPropertyValueBean();
        }
        return propertyValue.getStrValue();
    }
    
}
