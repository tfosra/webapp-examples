package test;

import com.tfosra.restau.backend.srv.itf.IRestaurantSRV;
import org.json.JSONObject;
import org.springframework.context.ApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 *
 * @author ShadowHacker
 */
public class Test {
    
    public static void main(String[] args) throws Exception {
        ApplicationContext context = new ClassPathXmlApplicationContext("spring-srv.xml");
//        IObjObjectSRV srv = (IObjObjectSRV) context.getBean("ObjObjectSRV");
        IRestaurantSRV srv = (IRestaurantSRV) context.getBean("RestaurantSRV");
//        srv.get();
//        System.out.println(srv.getMessage());
        JSONObject json = new JSONObject();
        json.put("strName", "White House");
        json.put("strDescription", "Grand restaurant situé à Douala-Akwa");
        json.put("strProperty", "chefName");
        json.put("objObjectName", "Restaurant");
        srv.test(json.toString());
    }
    
}
