/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.tfosra.restau.backend.srv.impl;

import com.tfosra.restau.backend.dao.itf.IObjObjectDAO;
import com.tfosra.restau.backend.dao.itf.IObjectPropertyDAO;
import com.tfosra.restau.backend.entity.ObjObject;
import com.tfosra.restau.backend.srv.itf.IObjObjectSRV;
import com.tfosra.restau.backend.entity.ObjectProperty;
import com.tfosra.restau.backend.exception.CustomException;
import java.util.List;
import org.json.JSONObject;

/**
 *
 * @author ShadowHacker
 */
public class ObjObjectSRVImpl extends BaseSRVImpl implements IObjObjectSRV {

    private IObjObjectDAO objObjectDAO;
    private IObjectPropertyDAO objectPropertyDAO;

    public void setObjObjectDAO(IObjObjectDAO objObjectDAO) {
        this.objObjectDAO = objObjectDAO;
    }

    public void setObjectPropertyDAO(IObjectPropertyDAO objectPropertyDAO) {
        this.objectPropertyDAO = objectPropertyDAO;
    }
    
    @Override
    public void get() throws Exception {
        try {
            List<ObjObject> lst = objObjectDAO.get();
            this.setError("1");
            this.setMessage(buildJsonFromEntityList(lst));
        } catch (Exception e) {
            handleError(e);
            throw new CustomException(e);
        }
    }
    
    @Override
    public void getProperties() throws Exception {
        try {
            List<ObjectProperty> lst = objectPropertyDAO.get();
            this.setError("1");
            this.setMessage(buildJsonFromEntityList(lst));
        } catch (Exception e) {
            handleError(e);
            throw new CustomException(e);
        }
    }

    @Override
    public void createProperty(String jsonParam) throws Exception {
        try {
            JSONObject json = new JSONObject(jsonParam);
            ObjectProperty r = objectPropertyDAO.create(json);
            this.setError("1");
            this.setMessage(buildJsonFromEntity(r));
        } catch (Exception e) {
            handleError(e);
            throw new CustomException(e);
        }
    }

    @Override
    public void updateProperty(String intId, String jsonParam) throws Exception {
        try {
            JSONObject json = new JSONObject(jsonParam);
            ObjectProperty r = objectPropertyDAO.update(Integer.valueOf(intId), json);
            this.setError("1");
            this.setMessage(buildJsonFromEntity(r));
        } catch (Exception e) {
            this.setError("0");
            this.setMessage(e.getMessage());
            throw new CustomException(e);
        }
    }

    @Override
    public void deleteProperty(String intId) throws Exception {
        try {
            objectPropertyDAO.delete(Integer.valueOf(intId));
            this.setError("1");
            this.setMessage("Suppression avec succes");
        } catch (Exception e) {
            this.setError("0");
            this.setMessage(e.getMessage());
            throw new CustomException(e);
        }
    }

    @Override
    public void getPropertyById(String intId) throws Exception {
        try {
            ObjectProperty r = objectPropertyDAO.getById(Integer.valueOf(intId));
            this.setError("1");
            this.setMessage(buildJsonFromEntity(r));
        } catch (Exception e) {
            this.setError("0");
            this.setMessage(e.getMessage());
            throw new CustomException(e);
        }
    }

}
