
/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package com.tfosra.restau.backend.srv.impl;

import com.tfosra.restau.backend.dao.itf.IObjectPropertyDAO;
import com.tfosra.restau.backend.srv.itf.IRestaurantSRV;
import com.tfosra.restau.backend.dao.itf.IRestaurantDAO;
import com.tfosra.restau.backend.entity.ObjectProperty;
import com.tfosra.restau.backend.entity.Restaurant;
import com.tfosra.restau.backend.exception.CustomException;
import com.tfosra.restau.backend.exception.CustomExceptionChild;
import java.util.List;
import org.json.JSONObject;

/**
 *
 * @author ShadowHacker
 */
public class RestaurantSRVImpl extends BaseSRVImpl implements IRestaurantSRV {

    private IRestaurantDAO restaurantDAO;
    private IObjectPropertyDAO objectPropertyDAO;

    public void setRestaurantDAO(IRestaurantDAO restaurantDAO) {
        this.restaurantDAO = restaurantDAO;
    }

    public void setObjectPropertyDAO(IObjectPropertyDAO objectPropertyDAO) {
        this.objectPropertyDAO = objectPropertyDAO;
    }
    
    @Override
    public void get() throws Exception {
        try {
            List<Restaurant> lst = restaurantDAO.get();
            this.setError("1");
            this.setMessage(buildJsonFromEntityList(lst));
        } catch (Exception e) {
            handleError(e);
            throw new CustomException(e);
        }
    }

    @Override
    public void create(String jsonParam) throws Exception {
        try {
            JSONObject json = new JSONObject(jsonParam);
            Restaurant r = restaurantDAO.create(json);
            this.setError("1");
            this.setMessage(buildJsonFromEntity(r));
        } catch (Exception e) {
            handleError(e);
            throw new CustomException(e);
        }
    }
    
    @Override
    public void test(String jsonParam) throws Exception {
        try {
            JSONObject json = new JSONObject(jsonParam);
            Restaurant r = restaurantDAO.create(json);
            ObjectProperty p = objectPropertyDAO.create(json);
            throw new CustomExceptionChild(new Exception("A spider has been found in your code!"));
//            this.setError("1");
//            this.setMessage(buildJsonFromEntity(r));
        } catch (Exception e) {
            handleError(e);
//            throw new CustomException(e);
            throw e;
        }
    }

    @Override
    public void update(String intId, String jsonParam) throws Exception {
        try {
            JSONObject json = new JSONObject(jsonParam);
            Restaurant r = restaurantDAO.update(Integer.valueOf(intId), json);
            this.setError("1");
            this.setMessage(buildJsonFromEntity(r));
        } catch (Exception e) {
            this.setError("0");
            this.setMessage(e.getMessage());
            throw new CustomException(e);
        }
    }

    @Override
    public void delete(String intId) throws Exception {
        try {
            restaurantDAO.delete(Integer.valueOf(intId));
            this.setError("1");
            this.setMessage("Suppression avec succes");
        } catch (Exception e) {
            this.setError("0");
            this.setMessage(e.getMessage());
            throw new CustomException(e);
        }
    }

    @Override
    public void getById(String intId) throws Exception {
        try {
            Restaurant r = restaurantDAO.getById(Integer.valueOf(intId));
            this.setError("1");
            this.setMessage(buildJsonFromEntity(r));
        } catch (Exception e) {
            this.setError("0");
            this.setMessage(e.getMessage());
            throw new CustomException(e);
        }
    }
    
}
