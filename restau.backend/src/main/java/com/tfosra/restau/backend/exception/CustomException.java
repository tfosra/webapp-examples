/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.tfosra.restau.backend.exception;

/**
 *
 * @author ShadowHacker
 */
public class CustomException extends Exception {

    public CustomException(Throwable cause) {
        super(cause.getMessage());
    }
    
}
