/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package com.tfosra.restau.backend.srv.itf;

/**
 *
 * @author ShadowHacker
 */
public interface IBaseSRV {
    
    public String getMessage();
    
    public String getError();
    
}
