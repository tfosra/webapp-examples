/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.tfosra.restau.backend.dao.impl;

import com.tfosra.restau.backend.dao.itf.IObjObjectDAO;
import com.tfosra.restau.backend.entity.ObjObject;
import com.tfosra.restau.backend.exception.CustomException;
import java.text.MessageFormat;
import java.util.List;
import org.apache.log4j.Logger;
import org.json.JSONException;
import org.json.JSONObject;
import org.springframework.dao.DataAccessException;

/**
 *
 * @author ShadowHacker
 */
public class ObjObjectDAOImpl extends BaseDAO implements IObjObjectDAO {

    private static final Logger log = Logger.getLogger(ObjObjectDAOImpl.class.getName());

    @Override
    public ObjObject create(JSONObject jsonParam) throws Exception {
        try {
            String strName = jsonParam.getString("strName");

            ObjObject o = new ObjObject();
            o.setStrName(strName);
            create_(o);
            return o;
        } catch (JSONException | DataAccessException e) {
            log.error(e.getMessage());
            throw new CustomException(e);
        }
    }

    @Override
    public ObjObject update(Integer id, JSONObject jsonParam) throws Exception {
        try {
            ObjObject o = getById(id);
            String strName = jsonParam.optString("strName", null);
            boolean updated = false;

            if (strName != null) {
                o.setStrName(strName);
                updated = true;
            }

            if (updated) {
                update_(o);
            }
            return o;
        } catch (Exception e) {
            log.error(e.getMessage());
            throw new CustomException(e);
        }
    }
    
    @Override
    public List<ObjObject> get() throws Exception {
        try {
            return super.get(ObjObject.class);
        } catch (Exception e) {
            log.error(e.getMessage());
            throw new CustomException(e);
        }
    }

    @Override
    public ObjObject getById(Integer id) throws Exception {
        try {
            ObjObject r = super.getUniqueByField(ObjObject.class, "id", id);
            if (r == null) {
                throw new Exception(MessageFormat.format("ObjObject \"{0}\" not found", id));
            }
            return r;
        } catch (Exception e) {
            log.error(e.getMessage());
            throw new CustomException(e);
        }
    }
    
    @Override
    public ObjObject getByName(String name) throws Exception {
        try {
            return super.getUniqueByField(ObjObject.class, "strName", name);
        } catch (Exception e) {
            log.error(e.getMessage());
            throw new CustomException(e);
        }
    }

    @Override
    public boolean delete(Integer id) throws Exception {
        try {
            ObjObject r = getById(id);
            delete_(r);
            return true;
        } catch (Exception e) {
            log.error(e.getMessage());
            throw new CustomException(e);
        }
    }

}
