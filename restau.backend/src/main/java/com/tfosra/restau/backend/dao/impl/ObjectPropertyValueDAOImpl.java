/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.tfosra.restau.backend.dao.impl;

import com.tfosra.restau.backend.dao.itf.IObjectPropertyValueDAO;
import com.tfosra.restau.backend.dao.itf.IObjectPropertyDAO;
import com.tfosra.restau.backend.entity.ObjectProperty;
import com.tfosra.restau.backend.entity.ObjectPropertyValue;
import com.tfosra.restau.backend.exception.CustomException;
import java.text.MessageFormat;
import java.util.List;
import org.apache.log4j.Logger;
import org.hibernate.criterion.Restrictions;
import org.json.JSONObject;

/**
 *
 * @author ShadowHacker
 */
public class ObjectPropertyValueDAOImpl extends BaseDAO implements IObjectPropertyValueDAO {

    private static final Logger log = Logger.getLogger(ObjectPropertyValueDAOImpl.class.getName());
    private IObjectPropertyDAO objectPropertyDAO;

    public void setObjectPropertyDAO(IObjectPropertyDAO objectPropertyDAO) {
        this.objectPropertyDAO = objectPropertyDAO;
    }

    @Override
    public ObjectPropertyValue create(JSONObject jsonParam) throws Exception {
        try {
            String strValue = jsonParam.getString("strValue");
            int intItem = jsonParam.getInt("intItem");
            int objectPropertyId = jsonParam.optInt("objectPropertyId", -1);
            
            ObjectPropertyValue o = new ObjectPropertyValue();
            o.setStrValue(strValue);
            o.setIntItem(intItem);
            
            if (objectPropertyId > -1) {
                ObjectProperty oObjectProperty = null;
                if (objectPropertyId > 0) {
                    objectPropertyDAO.getById(objectPropertyId);
                }
                o.setObjectPropertyId(oObjectProperty);
            }
            create_(o);
            return o;
        } catch (Exception e) {
            log.error(e.getMessage());
            throw new CustomException(e);
        }
    }

    @Override
    public ObjectPropertyValue update(Integer id, JSONObject jsonParam) throws Exception {
        try {
            String strValue = jsonParam.optString("strValue", null);
            boolean updated = false;

            ObjectPropertyValue o = getById(id);
            if (strValue != null) {
                o.setStrValue(strValue);
                updated = true;
            }

            if (updated) {
                update_(o);
            }
            return o;
        } catch (Exception e) {
            log.error(e.getMessage());
            throw new CustomException(e);
        }
    }
    
    @Override
    public List<ObjectPropertyValue> get() throws Exception {
        try {
            return super.get(ObjectPropertyValue.class);
        } catch (Exception e) {
            log.error(e.getMessage());
            throw new CustomException(e);
        }
    }

    @Override
    public ObjectPropertyValue getById(Integer id) throws Exception {
        try {
            ObjectPropertyValue r = super.getUniqueByField(ObjectPropertyValue.class, "id", id);
            if (r == null) {
                throw new Exception(MessageFormat.format("ObjectPropertyValue \"{0}\" not found", id));
            }
            return r;
        } catch (Exception e) {
            log.error(e.getMessage());
            throw new CustomException(e);
        }
    }

    @Override
    public List<ObjectPropertyValue> getByProperties(List<ObjectProperty> lstOfProperty, Integer intItem) throws Exception {
        try {
            return createCriteria(ObjectPropertyValue.class)
                    .add(Restrictions.in("objectPropertyId", lstOfProperty))
                    .add(Restrictions.eq("intItem", intItem))
                    .list();
        } catch (Exception e) {
            log.error(e.getMessage());
            throw new CustomException(e);
        }
    }

    @Override
    public boolean delete(Integer id) throws Exception {
        try {
            ObjectPropertyValue r = getById(id);
            delete_(r);
            return true;
        } catch (Exception e) {
            log.error(e.getMessage());
            throw new CustomException(e);
        }
    }

}
