/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package com.tfosra.restau.backend.srv.itf;

/**
 *
 * @author ShadowHacker
 */
public interface IObjObjectSRV extends IBaseSRV {
    
    public void get() throws Exception;
    public void getProperties() throws Exception;
    public void createProperty(String jsonParam) throws Exception;
    public void updateProperty(String intId, String jsonParam) throws Exception;
    public void deleteProperty(String intId) throws Exception;
    public void getPropertyById(String intId) throws Exception;
    
}
