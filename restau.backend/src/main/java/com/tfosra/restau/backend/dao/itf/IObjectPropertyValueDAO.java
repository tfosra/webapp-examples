/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.tfosra.restau.backend.dao.itf;

import com.tfosra.restau.backend.entity.ObjectProperty;
import com.tfosra.restau.backend.entity.ObjectPropertyValue;
import java.util.List;
import org.json.JSONObject;


/**
 *
 * @author ShadowHacker
 */
public interface IObjectPropertyValueDAO {
    
    public ObjectPropertyValue create(JSONObject jsonParam) throws Exception;
    
    public ObjectPropertyValue update(Integer id, JSONObject jsonParam) throws Exception;
    
    public ObjectPropertyValue getById(Integer id) throws Exception;
    
    public List<ObjectPropertyValue> get() throws Exception;
    
    public List<ObjectPropertyValue> getByProperties(List<ObjectProperty> lstOfProperty, Integer intItem) throws Exception;
    
    public boolean delete(Integer id) throws Exception;
    
}
