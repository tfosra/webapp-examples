/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.tfosra.restau.backend.dao.impl;

import com.tfosra.restau.backend.dao.itf.IObjObjectDAO;
import com.tfosra.restau.backend.dao.itf.IObjectPropertyDAO;
import com.tfosra.restau.backend.entity.ObjObject;
import com.tfosra.restau.backend.entity.ObjectProperty;
import com.tfosra.restau.backend.exception.CustomException;
import java.text.MessageFormat;
import java.util.List;
import org.apache.log4j.Logger;
import org.json.JSONException;
import org.json.JSONObject;
import org.springframework.dao.DataAccessException;

/**
 *
 * @author ShadowHacker
 */
public class ObjectPropertyDAOImpl extends BaseDAO implements IObjectPropertyDAO {

    private static final Logger log = Logger.getLogger(ObjectPropertyDAOImpl.class.getName());
    
    private IObjObjectDAO objObjectDAO;

    public void setObjObjectDAO(IObjObjectDAO objObjectDAO) {
        this.objObjectDAO = objObjectDAO;
    }

    @Override
    public ObjectProperty create(JSONObject jsonParam) throws Exception {
        try {
            String strProperty = jsonParam.getString("strProperty");
            String objObjectName = jsonParam.optString("objObjectName");
            ObjObject oObjObject = objObjectDAO.getByName(objObjectName);

            ObjectProperty o = new ObjectProperty();
            o.setStrProperty(strProperty);
            o.setObjObjectId(oObjObject);
            create_(o);
            return o;
        } catch (JSONException | DataAccessException e) {
            log.error(e.getMessage());
            throw new CustomException(e);
        }
    }

    @Override
    public ObjectProperty update(Integer id, JSONObject jsonParam) throws Exception {
        try {
            ObjectProperty o = getById(id);
            String strProperty = jsonParam.optString("strProperty", null);
            boolean updated = false;

            if (strProperty != null) {
                o.setStrProperty(strProperty);
                updated = true;
            }

            if (updated) {
                update_(o);
            }
            return o;
        } catch (Exception e) {
            log.error(e.getMessage());
            throw new CustomException(e);
        }
    }
    
    @Override
    public List<ObjectProperty> get() throws Exception {
        try {
            return super.get(ObjectProperty.class);
        } catch (Exception e) {
            log.error(e.getMessage());
            throw new CustomException(e);
        }
    }

    @Override
    public ObjectProperty getById(Integer id) throws Exception {
        try {
            ObjectProperty r = super.getUniqueByField(ObjectProperty.class, "id", id);
            if (r == null) {
                throw new Exception(MessageFormat.format("ObjectProperty \"{0}\" not found", id));
            }
            return r;
        } catch (Exception e) {
            log.error(e.getMessage());
            throw new CustomException(e);
        }
    }

    @Override
    public List<ObjectProperty> getByObject(ObjObject oObjObject) throws Exception {
        try {
            return (List<ObjectProperty>) super.getListByField(ObjectProperty.class, "objObjectId", oObjObject);
        } catch (Exception e) {
            log.error(e.getMessage());
            throw new CustomException(e);
        }
    }

    @Override
    public boolean delete(Integer id) throws Exception {
        try {
            ObjectProperty r = getById(id);
            delete_(r);
            return true;
        } catch (Exception e) {
            log.error(e.getMessage());
            throw new CustomException(e);
        }
    }

}
