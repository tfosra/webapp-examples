/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.tfosra.restau.backend.dao.itf;

import com.tfosra.restau.backend.entity.ObjObject;
import com.tfosra.restau.backend.entity.ObjectProperty;
import java.util.List;
import org.json.JSONObject;

/**
 *
 * @author ShadowHacker
 */
public interface IObjectPropertyDAO {
    
    public ObjectProperty create(JSONObject jsonParam) throws Exception;
    
    public ObjectProperty update(Integer id, JSONObject jsonParam) throws Exception;
    
    public ObjectProperty getById(Integer id) throws Exception;
    
    public List<ObjectProperty> get() throws Exception;
    
    public List<ObjectProperty> getByObject(ObjObject oObjObject) throws Exception;
    
    public boolean delete(Integer id) throws Exception;
    
}
