/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.tfosra.restau.backend.dao.impl;

import com.tfosra.restau.backend.dao.itf.IRestaurantDAO;
import com.tfosra.restau.backend.entity.Restaurant;
import com.tfosra.restau.backend.exception.CustomException;
import java.text.MessageFormat;
import java.util.List;
import org.apache.log4j.Logger;
import org.json.JSONObject;

/**
 *
 * @author ShadowHacker
 */
public class RestaurantDAOImpl extends BaseDAO implements IRestaurantDAO {

    private static final Logger log = Logger.getLogger(RestaurantDAOImpl.class.getName());

    @Override
    public Restaurant create(JSONObject jsonParam) throws Exception {
        try {
            String strName = jsonParam.getString("strName");
            String strDescription = jsonParam.optString("strDescription");

            Restaurant r = new Restaurant();
            r.setStrName(strName);
            r.setStrDescription(strDescription);
            create_(r);
            return r;
        } catch (Exception e) {
            log.error(e.getMessage());
            throw new CustomException(e);
        }
    }

    @Override
    public Restaurant update(Integer id, JSONObject jsonParam) throws Exception {
        try {
            Restaurant r = getById(id);
            String strName = jsonParam.optString("strName", null);
            String strDescription = jsonParam.optString("strDescription", null);
            boolean updated = false;

            if (strName != null) {
                r.setStrName(strName);
                updated = true;
            }
            if (strDescription != null) {
                r.setStrDescription(strDescription);
                updated = true;
            }

            if (updated) {
                update_(r);
            }
            return r;
        } catch (Exception e) {
            log.error(e.getMessage());
            throw new CustomException(e);
        }
    }
    
    @Override
    public List<Restaurant> get() throws Exception {
        try {
            return super.get(Restaurant.class);
        } catch (Exception e) {
            log.error(e.getMessage());
            throw new CustomException(e);
        }
    }

    @Override
    public Restaurant getById(Integer id) throws Exception {
        try {
            Restaurant r = super.getUniqueByField(Restaurant.class, "id", id);
            if (r == null) {
                throw new Exception(MessageFormat.format("Restaurant \"{0}\" not found", id));
            }
            return r;
        } catch (Exception e) {
            log.error(e.getMessage());
            throw new CustomException(e);
        }
    }

    @Override
    public boolean delete(Integer id) throws Exception {
        try {
            Restaurant r = getById(id);
            delete_(r);
            return true;
        } catch (Exception e) {
            log.error(e.getMessage());
            throw new CustomException(e);
        }
    }
    
    public int totalRestaurants() throws Exception {
        try {
            
        } catch (Exception e) {
            log.error(e.getMessage());
            throw new CustomException(e);
        }
        return 0;
    }

}
