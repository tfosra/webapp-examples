/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.tfosra.restau.backend.srv.impl;

import com.tfosra.restau.backend.dao.itf.IObjObjectDAO;
import com.tfosra.restau.backend.dao.itf.IObjectPropertyDAO;
import com.tfosra.restau.backend.dao.itf.IObjectPropertyValueDAO;
import com.tfosra.restau.backend.entity.BaseEntity;
import com.tfosra.restau.backend.entity.ObjObject;
import com.tfosra.restau.backend.entity.ObjectProperty;
import com.tfosra.restau.backend.entity.ObjectPropertyValue;
import com.tfosra.restau.backend.entity.Restaurant;
import com.tfosra.restau.backend.srv.itf.IBaseSRV;
import java.lang.reflect.Method;
import java.util.List;
import org.apache.log4j.Logger;
import org.json.JSONArray;
import org.json.JSONObject;

/**
 *
 * @author ShadowHacker
 */
public class BaseSRVImpl implements IBaseSRV {

    private String message;
    private String error;

    private static Logger log = Logger.getLogger(BaseSRVImpl.class.getName());

    private IObjObjectDAO objObjectDAO0;
    private IObjectPropertyDAO objectPropertyDAO0;
    private IObjectPropertyValueDAO objectPropertyValueDAO0;

    public void setObjObjectDAO0(IObjObjectDAO objObjectDAO) {
        this.objObjectDAO0 = objObjectDAO;
    }

    public void setObjectPropertyDAO0(IObjectPropertyDAO objectPropertyDAO) {
        this.objectPropertyDAO0 = objectPropertyDAO;
    }

    public void setObjectPropertyValueDAO0(IObjectPropertyValueDAO objectPropertyValueDAO) {
        this.objectPropertyValueDAO0 = objectPropertyValueDAO;
    }

    @Override
    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    @Override
    public String getError() {
        return error;
    }

    public void setError(String error) {
        this.error = error;
    }

    public void handleError(Exception e) {
        setError("0");
        setMessage(e.getMessage());
    }

    protected <T extends BaseEntity> String buildJsonFromEntity(T oEntity) throws Exception {
        String objectName = oEntity.getClass().getSimpleName();
        String methodName = "buildJsonFrom" + objectName;

        Method buildMethod = BaseSRVImpl.class.getDeclaredMethod(methodName, oEntity.getClass());
        String strJson = (String) buildMethod.invoke(this, oEntity);

        JSONObject json = new JSONObject(strJson);
        buildJsonFromBaseEntity(oEntity, json);
        return json.toString();
    }

    protected <T extends BaseEntity> String buildJsonFromEntityList(List<T> lstOfEntity) throws Exception {
        JSONArray array = new JSONArray();
        for (T oEntity : lstOfEntity) {
            array.put(buildJsonFromEntity(oEntity));
        }
        return array.toString();
    }

    private <T extends BaseEntity> void buildJsonFromBaseEntity(T oEntity, JSONObject json) throws Exception {
        json.put("id", oEntity.getId());
        System.out.println("Building base from " + oEntity);
        String objectName = oEntity.getClass().getSimpleName();
        ObjObject object = objObjectDAO0.getByName(objectName);
        if (object != null) {
            List<ObjectProperty> lstOfProperties = objectPropertyDAO0.getByObject(object);
            List<ObjectPropertyValue> lstOfValues = objectPropertyValueDAO0.getByProperties(lstOfProperties, oEntity.getId());

            JSONArray array = new JSONArray();
            for (ObjectPropertyValue value : lstOfValues) {
                array.put(buildJsonFromEntity(value));
            }
            json.put("lstOfObjectPropertyValue", array.toString());
        }
    }

    private String buildJsonFromRestaurant(Restaurant r) {
        JSONObject json = new JSONObject();
        json.put("strName", r.getStrName());
        json.put("strDescription", r.getStrDescription());
        return json.toString();
    }

    private String buildJsonFromObjObject(ObjObject o) throws Exception {
        JSONObject json = new JSONObject();
        json.put("strName", o.getStrName());
        
        JSONArray array = new JSONArray();
        if (o.getObjectPropertyCollection() != null) {
            for (ObjectProperty prop : o.getObjectPropertyCollection()) {
                array.put(buildJsonFromEntity(prop));
            }
        }
        json.put("lstOfObjectProperty", array.toString());
        return json.toString();
    }

    private String buildJsonFromObjectProperty(ObjectProperty o) {
        JSONObject json = new JSONObject();
        json.put("strProperty", o.getStrProperty());
        json.put("objObjectId", o.getObjObjectId().getId());
        json.put("objObjectName", o.getObjObjectId().getStrName());
        return json.toString();
    }

    private String buildJsonFromObjectPropertyValue(ObjectPropertyValue o) {
        JSONObject json = new JSONObject();
        json.put("objectPropertyId", o.getObjectPropertyId().getId());
        json.put("strValue", o.getStrValue());
        json.put("intItem", o.getIntItem());
        return json.toString();
    }

}
