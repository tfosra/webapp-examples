/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package com.tfosra.restau.backend.srv.itf;

/**
 *
 * @author ShadowHacker
 */
public interface IRestaurantSRV extends IBaseSRV {
    
    public void get() throws Exception;
    public void create(String jsonParam) throws Exception;
    public void test(String jsonParam) throws Exception;
    public void update(String intId, String jsonParam) throws Exception;
    public void delete(String intId) throws Exception;
    public void getById(String intId) throws Exception;
    
}
