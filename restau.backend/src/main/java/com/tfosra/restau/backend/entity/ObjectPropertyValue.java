/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.tfosra.restau.backend.entity;

import java.io.Serializable;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author ShadowHacker
 */
@Entity
@Table(name = "\"ObjectPropertyValue\"")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "ObjectPropertyValue.findAll", query = "SELECT o FROM ObjectPropertyValue o"),
    @NamedQuery(name = "ObjectPropertyValue.findById", query = "SELECT o FROM ObjectPropertyValue o WHERE o.id = :id"),
    @NamedQuery(name = "ObjectPropertyValue.findByStrValue", query = "SELECT o FROM ObjectPropertyValue o WHERE o.strValue = :strValue"),
    @NamedQuery(name = "ObjectPropertyValue.findByIntItem", query = "SELECT o FROM ObjectPropertyValue o WHERE o.intItem = :intItem")})
public class ObjectPropertyValue extends BaseEntity implements Serializable {
    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "id")
    private Integer id;
    @Size(max = 255)
    @Column(name = "\"strValue\"")
    private String strValue;
    @Basic(optional = false)
    @NotNull
    @Column(name = "\"intItem\"")
    private int intItem;
    @JoinColumn(name = "\"ObjectPropertyId\"", referencedColumnName = "id")
    @ManyToOne(optional = false)
    private ObjectProperty objectPropertyId;

    public ObjectPropertyValue() {
    }

    public ObjectPropertyValue(Integer id) {
        this.id = id;
    }

    public ObjectPropertyValue(Integer id, int intItem) {
        this.id = id;
        this.intItem = intItem;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getStrValue() {
        return strValue;
    }

    public void setStrValue(String strValue) {
        this.strValue = strValue;
    }

    public int getIntItem() {
        return intItem;
    }

    public void setIntItem(int intItem) {
        this.intItem = intItem;
    }

    public ObjectProperty getObjectPropertyId() {
        return objectPropertyId;
    }

    public void setObjectPropertyId(ObjectProperty objectPropertyId) {
        this.objectPropertyId = objectPropertyId;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (id != null ? id.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof ObjectPropertyValue)) {
            return false;
        }
        ObjectPropertyValue other = (ObjectPropertyValue) object;
        if ((this.id == null && other.id != null) || (this.id != null && !this.id.equals(other.id))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "com.tfosra.restau.backend.entity.ObjectPropertyValue[ id=" + id + " ]";
    }
    
}
