/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.tfosra.restau.backend.utils;

import com.tfosra.restau.backend.entity.BaseEntity;
import java.util.logging.Level;
import java.util.logging.Logger;
import org.json.JSONObject;

/**
 *
 * @author ShadowHacker
 */
public class Utils {
    
    public <T extends BaseEntity> void setField(T oEntity, String fieldName, Object value) {
        try {
            if (oEntity == null) {
                return;
            }
            oEntity.getClass().getDeclaredField(fieldName).set(oEntity, value);
        } catch (NoSuchFieldException | SecurityException | IllegalArgumentException | IllegalAccessException ex) {
            Logger.getLogger(Utils.class.getName()).log(Level.SEVERE, null, ex);
        }
    }
    
    public <T extends BaseEntity, U> U getField(T oEntity, String fieldName) {
        try {
            if (oEntity == null) {
                return null;
            }
            return (U) oEntity.getClass().getDeclaredField(fieldName).get(oEntity);
        } catch (NoSuchFieldException | SecurityException | IllegalArgumentException | IllegalAccessException ex) {
            Logger.getLogger(Utils.class.getName()).log(Level.SEVERE, null, ex);
        }
        return null;
    }
    
}
