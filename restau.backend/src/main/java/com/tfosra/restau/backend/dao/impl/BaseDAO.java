/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.tfosra.restau.backend.dao.impl;

import com.tfosra.restau.backend.entity.BaseEntity;
import java.util.List;
import org.hibernate.Criteria;
import org.hibernate.Query;
import org.hibernate.SessionFactory;
import org.hibernate.classic.Session;
import org.hibernate.criterion.Restrictions;
import org.springframework.orm.hibernate3.HibernateTemplate;

public class BaseDAO extends HibernateTemplate {
    
    private SessionFactory sessionFactory;
    
    public Session thisSession() {
        return this.getSessionFactory().getCurrentSession();
    }
    
    public <T> void create_(T o) {
        thisSession().save(o);
    }
    
    public <T> T update_(T o) {
        return (T) thisSession().merge(o);
    }
    
    public <T> void delete_(T o) {
        thisSession().delete(o);
    }

    public Query createQuery(String query) {
        return thisSession().createQuery(query);
    }
    
    public Criteria createCriteria(String query) {
        return thisSession().createCriteria(query);
    }
    
    public Criteria createCriteria(Class type) {
        return thisSession().createCriteria(type);
    }
    
    protected <T extends BaseEntity> List<T> get(Class<T> type) throws Exception {
        return createCriteria(type).list();
    }
    
    protected <T extends BaseEntity> T getUniqueByField(Class<T> type, String fieldName, Object value) throws Exception {
        return (T) createCriteria(type)
                .add(Restrictions.eq(fieldName, value))
                .uniqueResult();
    }
    
    protected <T extends BaseEntity> List<T> getListByField(Class<T> type, String fieldName, Object value) throws Exception {
        return createCriteria(type)
                .add(Restrictions.eq(fieldName, value))
                .list();
    }
    
}
