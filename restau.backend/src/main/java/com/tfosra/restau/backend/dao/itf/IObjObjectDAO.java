/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.tfosra.restau.backend.dao.itf;

import com.tfosra.restau.backend.entity.ObjObject;
import java.util.List;
import org.json.JSONObject;

/**
 *
 * @author ShadowHacker
 */
public interface IObjObjectDAO {
    
    public ObjObject create(JSONObject jsonParam) throws Exception;
    
    public ObjObject update(Integer id, JSONObject jsonParam) throws Exception;
    
    public ObjObject getById(Integer id) throws Exception;
    
    public ObjObject getByName(String name) throws Exception;
    
    public List<ObjObject> get() throws Exception;
    
    public boolean delete(Integer id) throws Exception;
    
}
