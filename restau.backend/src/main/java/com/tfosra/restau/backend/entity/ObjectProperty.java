/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.tfosra.restau.backend.entity;

import java.io.Serializable;
import java.util.Collection;
import javax.persistence.Basic;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;

/**
 *
 * @author ShadowHacker
 */
@Entity
@Table(name = "\"ObjectProperty\"")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "ObjectProperty.findAll", query = "SELECT o FROM ObjectProperty o"),
    @NamedQuery(name = "ObjectProperty.findById", query = "SELECT o FROM ObjectProperty o WHERE o.id = :id"),
    @NamedQuery(name = "ObjectProperty.findByStrProperty", query = "SELECT o FROM ObjectProperty o WHERE o.strProperty = :strProperty")})
public class ObjectProperty extends BaseEntity implements Serializable {
    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "id")
    private Integer id;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 50)
    @Column(name = "\"strProperty\"")
    private String strProperty;
    @JoinColumn(name = "\"ObjObjectId\"", referencedColumnName = "id")
    @ManyToOne(optional = false)
    private ObjObject objObjectId;
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "objectPropertyId")
    private Collection<ObjectPropertyValue> objectPropertyValueCollection;

    public ObjectProperty() {
    }

    public ObjectProperty(Integer id) {
        this.id = id;
    }

    public ObjectProperty(Integer id, String strProperty) {
        this.id = id;
        this.strProperty = strProperty;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getStrProperty() {
        return strProperty;
    }

    public void setStrProperty(String strProperty) {
        this.strProperty = strProperty;
    }

    public ObjObject getObjObjectId() {
        return objObjectId;
    }

    public void setObjObjectId(ObjObject objObjectId) {
        this.objObjectId = objObjectId;
    }

    @XmlTransient
    public Collection<ObjectPropertyValue> getObjectPropertyValueCollection() {
        return objectPropertyValueCollection;
    }

    public void setObjectPropertyValueCollection(Collection<ObjectPropertyValue> objectPropertyValueCollection) {
        this.objectPropertyValueCollection = objectPropertyValueCollection;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (id != null ? id.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof ObjectProperty)) {
            return false;
        }
        ObjectProperty other = (ObjectProperty) object;
        if ((this.id == null && other.id != null) || (this.id != null && !this.id.equals(other.id))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "com.tfosra.restau.backend.entity.ObjectProperty[ id=" + id + " ]";
    }
    
}
