/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package com.tfosra.restau.backend.dao.itf;

import com.tfosra.restau.backend.entity.Restaurant;
import java.util.List;
import org.json.JSONObject;

/**
 *
 * @author ShadowHacker
 */
public interface IRestaurantDAO {
    
    public List<Restaurant> get() throws Exception;
    
    public Restaurant create(JSONObject jsonParam) throws Exception;
    
    public Restaurant update(Integer id, JSONObject jsonParam) throws Exception;
    
    public Restaurant getById(Integer id) throws Exception;
    
    public boolean delete(Integer id) throws Exception;
    
}
